minimized_atomic_memory_operations_allocated_K.a.txt: atomic_memory_operations_K.a.txt arm64-insn-group-minimizer.py .venv
	. .venv/bin/activate; \
	./arm64-insn-group-minimizer.py $< > .tmp
	mv .tmp $@

.venv:
	python3 -m venv .venv
	. .venv/bin/activate; \
	python3 -m pip install sympy

.PHONY: clean
clean:
	rm -rf .venv minimized_atomic_memory_operations_allocated_K
