# arm64-insn-group-minimizer

Use [SymPy](https://sympy.org/) to generate a minimized boolean
expression matching a group of instructions. For example, the
minimized form of the [aarch64 atomic instructions which are actually
atomics](atomic_memory_operations_K.a.txt) can be seen
[here](minimized_atomic_memory_operations_allocated_K.a.txt).

## Usage

```
make
...
cat minimized_atomic_memory_operations_allocated_K.a.txt
```