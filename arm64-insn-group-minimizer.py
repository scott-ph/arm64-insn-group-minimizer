#!/usr/bin/env python3
# SPDX-License-Identifier: BSD-2-Clause
# Copyright 2024 Ampere Computing LLC

import functools
import operator
import sympy
import sys


def main(fname):
    # Fill `bools` with (mask, val, comment) tuples for the
    # instructions to match
    bools = []
    with open(fname) as f:
        for l in f.readlines():
            if l.strip().startswith('#'):
                continue
            val, comment = (l.strip().split(' ', maxsplit=1) + [''])[:2]
            mask = int(val.replace('0', '1').replace('x', '0'), base=2)
            val = int(val.replace('x', '0'), base=2)
            bools.append((mask, val, comment))

    # `bits` holds the symbolic variables to be used in expressions
    bits = sympy.symbols(','.join(f'bit{n}' for n in range(32)))
    bit_index = {b: n for n, b in enumerate(bits)}

    # Fill `exprs` with boolean expressions that match
    # `(input & mask) == val` for each mask, val in `bools`
    exprs = []
    for mask, val, comment in bools:
        expr = []
        for i in range(32):
            if mask & (1 << i):
                if val & (1 << i):
                    expr.append(bits[i])
                else:
                    expr.append(~bits[i])
        exprs.append(functools.reduce(operator.and_, expr))

    # `expr` is the OR of each expression in `exprs`
    expr = functools.reduce(operator.or_, exprs)

    # Ask sympy to simplify `expr`
    expr = sympy.simplify_logic(expr, form='dnf', force=True)

    # expr is in disjunctive normal form (OR of ANDs), so extract the
    # ANDs into `exprs`. Or if there was just a single AND, then make
    # exprs a list of that one AND.
    exprs = expr.args if isinstance(expr, sympy.Or) else [expr]

    # Fill `tests` with (mask, val) tuples which are equivalent to the
    # AND expressions in the now minimized `exprs`
    tests = []
    for expr in exprs:
        mask = 0
        val = 0
        bits = expr.args if isinstance(expr, sympy.And) else [expr]
        for bit in bits:
            mask_bit = bit.args[0] if isinstance(bit, sympy.Not) else bit
            val_bit = (1 << bit_index[mask_bit]) if isinstance(
                bit, sympy.Symbol) else 0
            mask_bit = 1 << bit_index[mask_bit]
            mask |= mask_bit
            val |= val_bit
        comments = []
        for idx, (inmask, inval, incomment) in reversed(list(enumerate(bools))):
            if inmask & mask == mask and mask & inval == val:
                comments.insert(0, incomment)
                del(bools[idx])
        comment = (' // ' if comments else '') + '\n                                        // '.join(comments)
        tests.append(f' ((insn & {mask:#010x}) == {val:#010x}) ||{comment}')

    if tests:
        tests[0] = tests[0].replace(' ', '(', 1)
        tests[-1] = tests[-1].replace(' ||', ')  ', 1).rstrip()

    # Finally, print out the minimized (mask, val) tuples as a C-ish
    # expression.
    print('\n'.join(tests))


if __name__ == '__main__':
    sys.exit(main(*sys.argv[1:]))
